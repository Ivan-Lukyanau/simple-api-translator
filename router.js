const axios = require('axios');
const config = require('./public/js/config/config');

function getRandom(min, max) {
    return parseInt(Math.random() * (max - min + 1)+ min);
}

module.exports = function(app){
    
    app.get('/api/lingua', function(req, res, next){
        // get external data
        var words = req.param('words');        
        var isLatin = (req.param('latin')).toLowerCase();
        
        var url;
        // encode words
        if(isLatin === "true"){ // change to Boolean(isLatin)
            url = encodeURI(config.LINGUA_API_ENG_RU + words);            
        } else {
            url = config.LINGUA_API_RU_ENG + words + "&source=ru&target=en";
        }

        if(isLatin === "true"){
            axios.get(url)
            .then(response => {
                const respResult = response.data.translate[0].value;
                res.send(respResult);
            })
            .catch(err => {
                console.log(err);
            });
        } else {
            
            axios.get(url)
            .then(response => {
                const respResult = response.data.translation;
                res.send(respResult);
            })
            .catch(err => {
                console.log(err);
            });
        }
    });

    app.get('/api/images', function(req, res, next){
        var word = req.param('word');
        // config for cognitive dev service to search images
        var configForImageService = {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Ocp-Apim-Subscription-Key': config.IMAGE_SUBSCRIPTION_KEY,
            }
        };

        axios.get('https://api.cognitive.microsoft.com/bing/v5.0/images/search?q=' + encodeURI(word), configForImageService)
        .then(response => {
                var arrayResults = response.data.value; // get array of images
                var resultLength = arrayResults.length; // length of array
                var indexInteresting = getRandom(0,resultLength); // get random index
                var imageResult = arrayResults[indexInteresting].thumbnailUrl; // get thumbnail by index
                res.send(imageResult);
            })
            .catch(err => {
                console.log(err);
            });;

    });

    app.get('/*', function(req, res) {
        res.sendfile('./public/index.html');
    });
}