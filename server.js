const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const http = require('http');
const cors = require('cors');

const router = require('./router');

const app = express();
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));
app.use(cors({origin: '*'}));
app.use(bodyParser.json({ type: '*/*'}));
router(app);

const port = process.env.PORT || 3091;
const server = http.createServer(app);
server.listen(port);
//console.log('Server is running on port : ', port);