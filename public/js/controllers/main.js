angular.module('firstController', [])

.controller('firstController', function($scope, $http, Translator, StringHelper, StorageHelper){
    const enRu = "English <span class=\"glyphicon glyphicon-resize-horizontal\"></span> Russian";
    const ruEn = "Russian <span class=\"glyphicon glyphicon-resize-horizontal\"></span> English";

    $scope.formData = {};
    $scope.imageUrl = "";
    $scope.translateResult = "";
    $scope.translationDirection = enRu;

    $scope.translateWord = function(){
        if (!$.isEmptyObject($scope.formData)) {
            var requireWord = $scope.formData.words;
            Translator.translate(requireWord)
                .success(function(data){
                    $scope.formData = {};
                    $scope.translateResult = data;
                    
                    //lets move new word into localStorage
                    StorageHelper.addToHistory(requireWord /* initial */, data /* translated */); 

                    Translator.getImage(requireWord)
                    .success(function(response){ 
                        $scope.imageUrl = response;    
                    })
                    .error(err => { console.log('Error', err); });
                })
                .error(function(err){
                    console.log('Error:', err);
                });
        }
    };
    $scope.textChanged = function(){
        // whether words in latin or not?        
        var cyrillicResult = StringHelper.detectCyrillic($scope.formData.words);
        cyrillicResult == true
         ? $scope.translationDirection = ruEn
         : $scope.translationDirection = enRu;
    };
});