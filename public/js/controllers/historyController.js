angular.module('historyController', ['localStorageService'])
.controller('historyController', ['StorageHelper', function(StorageHelper){
    this.wordsArray = [];
    this.addWordToFavorites = (word) => {
        // lets add to favorites new word
        StorageHelper.addRemoveWordToFromFavorites(word)
        .then(resp => {
            this.getHistoryWordsArray();
        });
    }
     this.getHistoryWordsArray = () => {
        var historyResultArray = StorageHelper.getWordsFromHistory();
        if(historyResultArray){
            this.wordsArray = historyResultArray;
        }
    }
    this.cleanHistory = () => {
        localStorage.removeItem('myHistory');
        this.wordsArray = [];
    }
    this.getHistoryWordsArray();
}]);