angular.module('favoritesController', ['localStorageService'])
.controller('favoritesController', ['StorageHelper', function(StorageHelper){
    this.wordsArray = [];
    function getFavoritesWords(){
        //var result = StorageHelper.getMarkedWordsFromHistory();
        var result = StorageHelper.getWordsFromFavorites();
        if(result){
            this.wordsArray = result;
        }
    } // function declaration doesn't demand comma after curly braces end
    getFavoritesWords.apply(this);
}]);