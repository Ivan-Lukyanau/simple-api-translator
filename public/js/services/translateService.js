angular.module('translateService', [])
.factory('Translator', function($http, config){
    return {
        translate: function(words){
            var latinFlag = true;
            var encodedQuery = encodeURI(words);
            if(encodedQuery.startsWith('%')){
                latinFlag = false;
            }
            return $http({
                method: 'GET',
                url: '/api/lingua',
                params: {
                    words: latinFlag ? words : encodedQuery,
                    latin: latinFlag
                }
            });
        },

        getImage: function(words){
            var oneWord = words.split(/[\s,]+/)[0];
            return $http({
                method: 'GET',
                url: '/api/images',
                params: {
                    word: oneWord
                }
            });
        },
    }
});