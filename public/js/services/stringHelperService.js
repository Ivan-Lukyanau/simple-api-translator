angular.module('stringHelperService',[])
.factory('StringHelper', function(){
    return {
        detectCyrillic: function(string){
            return /^[\u0400-\u04FF ]+$/.test(string);
        },

        returnArrayWithoutEmptyElements: function(array){
            return array.filter(function(el) {return el.length != 0})
        },

        
    }
});