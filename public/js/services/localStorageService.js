angular.module('localStorageService', [])
.factory('StorageHelper', function(StringHelper, $q){
    return {

        addToHistory: function(initial, translated){
            var isCyrillic = StringHelper.detectCyrillic(initial);
            var direction = isCyrillic ? "RU-EN" : "EN-RU";

            var stringToStorage = initial+"|"+translated+"|"+direction+"|false";

            var currentHistory = localStorage.getItem("myHistory");
            if(currentHistory && currentHistory.length >= 0){
                var historyAsArray = currentHistory.split(/\r?\n?\:/);
                historyAsArray.push(stringToStorage);
                var joinedValuesFromArray = Array.prototype.join.call(historyAsArray, ":");
                localStorage.setItem("myHistory", joinedValuesFromArray);
            } else {
                localStorage.setItem("myHistory", stringToStorage);
            }
        },

        getWordsFromHistory: function(){
            var resultString = localStorage.getItem('myHistory');
            if(resultString){
                var historyAsArray = resultString.split(/\r?\n?\:/);
                var filteredResult =  historyAsArray.filter(function(el){ return el.length != 0});

                var structuredArray = [];
                for (var index = 0; index < filteredResult.length; index++) {
                    var element = filteredResult[index];
                    structuredArray.push(element.split("|"));
                }
                return structuredArray;
            }
        },

        getMarkedWordsFromHistory: function(){
            var resultString = localStorage.getItem('myHistory');
            if(resultString){
                var historyAsArray = resultString.split(/\r?\n?\:/);
                var filteredResult =  historyAsArray.filter(function(el){ return el.length != 0});

                var structuredArray = [];
                for (var index = 0; index < filteredResult.length; index++) {
                    
                    var element = filteredResult[index];
                    var splittedItem = element.split("|");
                    var needsToPush = splittedItem[splittedItem.length-1];
                    if(needsToPush == "true"){
                        structuredArray.push(splittedItem);
                    }
                }
                return structuredArray;
            }
        },

        addRemoveWordToFromFavorites: function(word, translation){
            var prom = $q.defer();
            var resultString = localStorage.getItem('myHistory');
            if(resultString){
                var historyAsArray = resultString.split(/\r?\n?\:/);
                var filteredResult =  historyAsArray.filter(function(el){ return el.length != 0});

                var structuredArray = [];

                for (var index = 0; index < filteredResult.length; index++) {
                    var element = filteredResult[index];
                    var splittedItem = element.split("|");
                    
                    if(splittedItem[0] === word){
                        if(splittedItem[splittedItem.length-1] == "true"){
                            splittedItem.splice(3,1);
                            splittedItem.push("false");
                            this.removeWordFromFavorites(word)
                            .then(resp => {
                            });
                        }else {
                            this.addWordToFavorites(splittedItem[0], splittedItem[1])
                            .then(resp => {
                            });
                            splittedItem.splice(3,1);
                            splittedItem.push("true");                   
                        }
                    }
                    var joinedTempArray = Array.prototype.join.call(splittedItem, "|");
                    structuredArray.push(joinedTempArray);
                }
                var joinedValuesFromArray = Array.prototype.join.call(structuredArray, ":");
                localStorage.removeItem('myHistory');
                localStorage.setItem('myHistory', joinedValuesFromArray);

                prom.resolve();
                return prom.promise;
            }
        },

        addWordToFavorites: function(word, translated){
            var prom = $q.defer();
            // word|translated|ru-en|true -- as result
            var stringToStorage = getStringToSave(word, translated, true);
            var currentFavorites = localStorage.getItem('myFavorites'); // get my Favorites collection
            if(currentFavorites && currentFavorites.length >=  0){ 
                var favoritesAsArray = currentFavorites.split(/\r?\n?\:/);
                var filteredResult =  favoritesAsArray.filter(function(el){ return el.length != 0});
                for (var index = 0; index < filteredResult.length; index++) {
                    var element = filteredResult[index];
                    var splittedItem = element.split("|");
                    if(splittedItem[0] === word){
                        prom.resolve();
                        return prom.promise; /* it is not necessary to add new one cause it already exists*/
                    }                    
                }
                favoritesAsArray.push(stringToStorage);
                var joinedValuesFromArray = Array.prototype.join.call(favoritesAsArray, ":");
                localStorage.removeItem('myFavorites');                
                localStorage.setItem("myFavorites", joinedValuesFromArray);
            } else { /* not exists collection */
                localStorage.setItem('myFavorites', stringToStorage);
            }
            prom.resolve();
            return prom.promise;
        },

        removeWordFromFavorites: function(word){
            var prom = $q.defer();
            var currentFavorites = getStringsArrayFromStorageByItemName('myFavorites');
            if(currentFavorites && currentFavorites.length >=  0){
                for (var index = 0; index < currentFavorites.length; index++) {
                    var element = currentFavorites[index];
                    var splittedItem = element.split("|");
                    if(splittedItem[0] === word){
                        currentFavorites.splice(index,1);
                        var joinedValuesFromArray = Array.prototype.join.call(currentFavorites, ":");
                        localStorage.removeItem("myFavorites");
                        localStorage.setItem("myFavorites", joinedValuesFromArray);
                         prom.resolve();
                         return prom.promise;
                    }
                }
            }            
            prom.resolve();
            return prom.promise;
        },

        /// GET WORDS FROM FAVORITES
        getWordsFromFavorites: function(){
             var resultString = localStorage.getItem('myFavorites');
            if(resultString){
                var favoritesAsArray = resultString.split(/\r?\n?\:/);
                var filteredResult =  favoritesAsArray.filter(function(el){ return el.length != 0});

                var structuredArray = [];
                for (var index = 0; index < filteredResult.length; index++) {
                    
                    var element = filteredResult[index];
                    var splittedItem = element.split("|");
                    structuredArray.push(splittedItem);
                }
                return structuredArray;
            }
        },
    };

    function getStringsArrayFromStorageByItemName(itemName){
        var myItem = localStorage.getItem(itemName);
        var historyAsArray = myItem.split(/\r?\n?\:/);
        var filteredResult =  historyAsArray.filter(function(el){ return el.length != 0});
        return filteredResult;
    }

    /// return element like an array of parts item represents as
    function getItemByWord(array, word){
        for (var index = 0; index < array.length; index++) {
           
            var element = array[index];
            var splittedItem = element.split("|");
            
            if(splittedItem[0] === word){
                return splittedItem;
            }
        }
    }

    function getStringToSave(initial, translated, flag){
        var isCyrillic = StringHelper.detectCyrillic(initial);
        var direction = isCyrillic ? "RU-EN" : "EN-RU";
        return initial+"|"+translated+"|"+direction+"|"+flag;
    }
});