angular.module('myApp', ['firstController', 'historyController', 'favoritesController', 'translateService', 'stringHelperService', 'localStorageService', 'ngAnimate', 'ui.router', 'ngSanitize'])
// configuring our constants 
// =============================================================================
.constant('config', {
    LINGUA_API: "http://api.lingualeo.com/gettranslates?word=",
})

// configuring our routes 
// =============================================================================
.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    
    $stateProvider
    
        .state('main', {
            url: '/',
            templateUrl: 'form.html'
        })

        .state({
            name: 'main.translator', 
            url: 'translator',
            templateUrl: 'translator.html',
            controller: 'firstController'
        })
        
        .state({
            name: 'main.favorites', 
            url: 'favorites',
            templateUrl: 'favorites.html',
            controller: 'favoritesController as favorites'
        })
        
        .state({
            name: 'main.history', 
            url: 'history',
            templateUrl: 'history.html',
            controller: 'historyController as history'
        });
        
    // use the HTML5 History API
    $locationProvider.html5Mode(true).hashPrefix('!');
    
    // catch all route
    // send users to the form page 
    $urlRouterProvider.otherwise('/');
});